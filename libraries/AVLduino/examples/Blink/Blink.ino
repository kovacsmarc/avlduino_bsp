/*

AVLduino blink program for testing LED outputs and user button.

*/

void led_setup()
{
  DDRB =  (1<<PB0) |
          (1<<PB1) |
          (1<<PB2) ;
  DDRC =  (1<<PC4) |
          (1<<PC5) ;
  DDRD =  (1<<PD2) |
          (1<<PD3) |
          (1<<PD4) |
          (1<<PD5) |
          (1<<PD6) |
          (1<<PD7) ;
}

void set_led(uint8_t led, uint8_t val)
{
    volatile uint8_t *port = PORTB;
    uint8_t portbit = 0;

    if(led < 1 || led > 11)
    {
      return;
    }
    
    switch(led)
    {
    case  1: port = &PORTB; portbit = 0; break;
    case  2: port = &PORTD; portbit = 7; break;
    case  3: port = &PORTB; portbit = 2; break;
    case  4: port = &PORTD; portbit = 6; break;
    case  5: port = &PORTB; portbit = 1; break;
    case  6: port = &PORTD; portbit = 5; break;
    case  7: port = &PORTD; portbit = 3; break;
    case  8: port = &PORTD; portbit = 2; break;
    case  9: port = &PORTD; portbit = 4; break;
    case 10: port = &PORTC; portbit = 5; break;
    case 11: port = &PORTC; portbit = 4; break;
    }

    if(val == 0)
    {
      *port |= (1<<portbit);
    }
    else
    {
      *port &= ~(1<<portbit);
    }
}

void set_all_led(uint8_t val)
{
  for(uint8_t i = 1; i <= 11; i++)
  {
    set_led(i, val);
  }
}

uint8_t get_usr_btn()
{
  return (PINC & (1<<3))?(1):(0);
}

void setup() 
{
  led_setup();
  set_all_led(0);
  Serial.begin(115200);

  delay(100);
}

void loop() 
{
  if(get_usr_btn())
  {
    set_all_led(0);
    set_led(6, 1);
  }
  else
  {
    set_all_led(1);
  }
  
  delay(500);

  if(get_usr_btn())
  {
    set_all_led(0);
    set_led(6, 0);
  }
  else
  {
    set_all_led(0);
  }
  
  delay(500);

  Serial.println("Hello AVL!");
  
}
